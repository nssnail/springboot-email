package com.sise;

import com.sise.service.EmailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes={EmailServiceApplication.class, EmailServiceTest.class})
public class EmailServiceTest {

    @Resource
    private EmailService emailService;

    @Test
    public void test(){
        emailService.sendSimpleMail("1191986647@qq.com","测试","测试");
    }
}